import ExamplesService from '../../services/examples.service.js';

export class Controller {
	byId(req, res) {
		ExamplesService.byId(req.params.id).then((entity) => {
			if (entity) {
				res.json(entity);
			}
			else {
				res.status(404).end();
			}
		});
	}
}

export default new Controller();
