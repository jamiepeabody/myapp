import Express from 'express';
import cookieParser from 'cookie-parser';
import * as path from 'path';
import bodyParser from 'body-parser';
import * as http from 'http';
import { fileURLToPath } from 'url';
import * as OpenApiValidator from 'express-openapi-validator';
import logger from './logger.js';
import errorHandler from '../api/middlewares/error.handler.js';

const app = new Express();

export default class ExpressServer {
	constructor() {
		const __filename = fileURLToPath(import.meta.url);
		const __dirname = path.dirname(__filename);
		const root = path.normalize(`${__dirname}/../..`);
		const apiSpec = path.join(__dirname, 'api.yml');
		const validateResponses = !!(
			process.env.OPENAPI_ENABLE_RESPONSE_VALIDATION &&
			process.env.OPENAPI_ENABLE_RESPONSE_VALIDATION.toLowerCase() === 'true'
		);
		logger.debug(`Validate responses: ${validateResponses}`);

		app.use(bodyParser.json({ limit: process.env.REQUEST_LIMIT || '100kb' }));
		app.use(
			bodyParser.urlencoded({
				extended: true,
				limit: process.env.REQUEST_LIMIT || '100kb',
			})
		);
		app.use(bodyParser.text({ limit: process.env.REQUEST_LIMIT || '100kb' }));
		app.use(cookieParser(process.env.SESSION_SECRET));

		this._public = `${root}/public`;
		app.use(Express.static(this._public));

		app.use(process.env.OPENAPI_SPEC || '/spec', Express.static(apiSpec));
		app.use(
			OpenApiValidator.middleware({
				apiSpec,
				validateResponses,
				ignorePaths: /.*\/spec(\/|$)/,
			})
		);
	}

	router(routes) {
		routes(app);
		app.use(errorHandler);
		return this;
	}

	listen(port = process.env.PORT) {
		const welcome = (port) => {
			const mode = process.env.NODE_ENV || 'development';
			logger.info(`Server started in ${mode}, listening on port: ${port}`);
			logger.info(`Access the OpenAPI spec at: http://localhost:${port}/api-explorer`);
			logger.info(`Access the API spec at: http://localhost:${port}/api/v1`);
		};

		http.createServer(app).listen(port, welcome(port));

		return app;
	}
}
