class ExamplesDatabase {
	constructor() {
		this._data = [];

		// Just example data
		this.insert({
			id: '123-45-6789',
			name: 'Jamie'
			// firstName: 'Joe',
			// lastName: 'Bloggs'
		});
	}

	all() {
		return Promise.resolve(this._data);
	}

	byId(id) {
		return Promise.resolve(this._data[id]);
	}

	insert({ id, ...stuff }) {
		const record = {
			id,
			...stuff
		};

		this._data[id] = record;

		return Promise.resolve(record);
	}
}

export default new ExamplesDatabase();
