import providersRouter from './api/controllers/providers/router.js';

export default function routes(app) {
	app.use('/api/v1/providers', providersRouter);
}
