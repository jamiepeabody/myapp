# myapp

## Installation

```bash
npm install
```

## Environment variables

The environment variables used to configure the service are found in `./.env`.

## Running

```bash
npm start
```

After startup, the OpenAPI explorer will be exposed on: http://localhost:3000/api-explorer/

The API will be exposed on: http://localhost:3000/api/v1. To hit the mocked API:

```bash
curl -H "accept: application/json"\
 -X GET\
 "http://localhost:3000/api/v1/providers/123-45-6789"
```

To test request validation, change the format of the identifier to something other than `XXX-XX-XXXX`.

## Development mode

In development mode, changes to the files will automatically restart the server.

```bash
npm run dev
```
