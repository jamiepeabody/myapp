import logger from '../../common/logger.js';
import db from './examples.db.service.js';

class ExamplesService {
	byId(id) {
		logger.info(`${this.constructor.name}.byId(${id})`);
		return db.byId(id);
	}
}

export default new ExamplesService();
