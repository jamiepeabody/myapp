import chai from 'chai';
import request from 'supertest';
import Server from '../server/index.js';

const expect = chai.expect;

describe('OpenAPI service', () => {
	it('should get provider by id', async () => {
		await request(Server)
			.get('/api/v1/providers/123-45-6789')
			.expect('Content-Type', /json/)
			.then((response) => {
				expect(response.body).to.deep.equal({
					id: '123-45-6789',
					firstName: 'Joe',
					lastName: 'Bloggs'
				});
			});
	});
});
